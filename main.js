


document.addEventListener('DOMContentLoaded', function() {


  let button = document.querySelector('.button');
  let openCloseBlock = document.querySelector('.open-close');

  button.addEventListener('click', function() {
    if(openCloseBlock.classList.contains('opened')) {
      openCloseBlock.classList.remove('opened');
    } else {
      openCloseBlock.classList.add('opened');
    }
  });

  
});
